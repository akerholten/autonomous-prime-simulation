﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zone : MonoBehaviour
{
    public enum ZoneStatus
    {
        Critical    = 1,
        High        = 2,
        Medium      = 3,
        Low         = 4
    }

    public ZoneStatus status;

    [SerializeField] private GameObject middleHexa;
    [SerializeField] private MeshRenderer middleHexaRenderer;
    // Start is called before the first frame update
    void Start()
    {
        if(middleHexa == null)
        {
            middleHexa = transform.GetChild(0).gameObject; // Might cause issues, fetches the first child
        }
        if (middleHexaRenderer == null)
            middleHexaRenderer = middleHexa.GetComponent<MeshRenderer>();
    }

    public void StartZoneSimulation(float updateInterval)
    {
        StartCoroutine(UpdateZone(updateInterval));
    }

    IEnumerator UpdateZone(float updateInterval)
    {
        while(SimulationManager.instance.SimulationIsRunning){
            yield return new WaitForSeconds(updateInterval);
            int zoneStatus = Mathf.RoundToInt(Random.Range(1.0f, 4.0f)); // TODO: Change this to some actual logic
            status = (ZoneStatus)zoneStatus;
            // The rounding might cause issues making certain zone statuses more likely than others atm

            if(zoneStatus == 1) 
            {
                middleHexaRenderer.material.color = new Color(1.0f, 0.0f, 0.0f, 0.17f); // red color
            }
            else if(zoneStatus == 2)
            {
                middleHexaRenderer.material.color = new Color(1.0f, 0.64f, 0.0f, 0.17f); // orange color
            }
            else if (zoneStatus == 3)
            {
                middleHexaRenderer.material.color = new Color(1.0f, 0.92f, 0.016f, 0.17f); // yellow color
            }
            else if (zoneStatus == 4)
            {
                middleHexaRenderer.material.color = new Color(0.0f, 1.0f, 0.0f, 0.17f); // green color
            }
        }
    }

    public void DisableRenderer()
    {
        middleHexaRenderer.enabled = false;
    }

    public void EnableRenderer()
    {
        middleHexaRenderer.enabled = true;
    }
}
