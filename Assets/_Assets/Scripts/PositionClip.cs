﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PositionClip : MonoBehaviour
{
    [SerializeField]
    public List<PlaybackNode<Vector3>> positionNodes;

    private void Start()
    {
        positionNodes = new List<PlaybackNode<Vector3>>();
    }

    public void AddNode(Vector3 position, float time)
    {
        if (positionNodes == null) positionNodes = new List<PlaybackNode<Vector3>>();
        Debug.Log("Node added with position: " + position + " at time: " + time + " as node nr " + positionNodes.Count + 1);
        PlaybackNode<Vector3> node = new PlaybackNode<Vector3>();
        node.Value = position;
        node.timePoint = time;
        positionNodes.Add(node);
    }

    public PlaybackNode<Vector3> GetLastNode()
    {
        if (positionNodes == null || positionNodes.Count <= 0) return null;

        return positionNodes[positionNodes.Count - 1];
    }


    void SaveToFile()
    {

    }

    void LoadFromFile(string path)
    {

    }
    
}
