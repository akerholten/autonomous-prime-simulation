﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PositionClip))]
public class PositionRecorder : MonoBehaviour
{
    public float distanceThreshold;
    public float timeRecorded;

    private float lastTime;

    bool _recording = false;
    [SerializeField] private PositionClip clip;

    private void Start()
    {
        if(clip == null)
        {
            Debug.LogError("Clip was null");
            clip = GetComponent<PositionClip>();
        }
    }

    void Update()
    {
        if (_recording)
        {
            timeRecorded += Time.timeSinceLevelLoad - lastTime;
            lastTime = Time.timeSinceLevelLoad;
            if (clip.positionNodes.Count <= 0)
            {
                clip.AddNode(transform.position, 0.0f); // Initial node
            }
            else if((clip.positionNodes[clip.positionNodes.Count - 1].Value - transform.position).sqrMagnitude > distanceThreshold)
            {
                clip.AddNode(transform.position, timeRecorded);
            }
        }
    }

    [ContextMenu("Start recording")]
    public void StartRecording()
    {
        if(clip.positionNodes.Count > 0)
        {
            Debug.LogError("Clip already contains something, remove before recording!");
            return;
        }

        timeRecorded = 0.0f;
        lastTime = Time.timeSinceLevelLoad;
        _recording = true;
        Debug.Log("Recording started!");
    }

    [ContextMenu("End recording")]
    public void EndRecording()
    {
        timeRecorded += Time.timeSinceLevelLoad - lastTime;
        lastTime = Time.timeSinceLevelLoad;

        clip.AddNode(transform.position, timeRecorded);

        _recording = false;

        Debug.Log("Recording ended!");
    }
}
