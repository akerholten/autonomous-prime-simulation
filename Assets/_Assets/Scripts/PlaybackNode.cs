﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class PlaybackNode<T>
{
    public T Value;
    public float timePoint;
}