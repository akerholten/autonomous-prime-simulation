﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarSpeed : MonoBehaviour
{
    [SerializeField] List<GameObject> beacons;

    [SerializeField] PositionPlayback positionPlayback;
    [SerializeField] LayerMask zoneLayerMask;

    Zone prevZone;

    private void Start()
    {
        if (positionPlayback == null)
            positionPlayback = GetComponent<PositionPlayback>();

        if(beacons == null || beacons.Count == 0)
        {
            Debug.LogWarning("Beacons list was nothing");
        }
    }

    void FixedUpdate()
    {
        UpdateZonesVisible();
        UpdateBeaconsVisible();
    }

    void UpdateZonesVisible()
    {
        RaycastHit[] hits = Physics.BoxCastAll(transform.position, new Vector3(0.1f, 0.1f, 0.1f), Vector3.down, Quaternion.identity, 3.5f, zoneLayerMask);

        float closestDistance = Mathf.Infinity;
        Zone closestZone = null;
        foreach (RaycastHit hit in hits)
        {
            float distance = (hit.transform.position - transform.position).sqrMagnitude;
            if (distance < closestDistance)
            {
                Zone zone = hit.transform.GetComponent<Zone>();
                if (zone == null)
                {
                    zone = hit.transform.GetComponentInParent<Zone>();
                    if (zone == null) Debug.LogError("Something went wrong, zone was null");
                }
                closestZone = zone;
                closestDistance = distance;
            }
        }
        if (closestZone != null)
        {
            closestZone.EnableRenderer();
            UpdateCarSpeed(closestZone);

            if (prevZone != null && prevZone != closestZone)
            {
                prevZone.DisableRenderer();
            }

            prevZone = closestZone;
        }
    }

    void UpdateBeaconsVisible()
    {
        float closestDistance = Mathf.Infinity;
        GameObject closestBeacon = null;

        foreach (GameObject beacon in beacons)
        {
            float distance = (beacon.transform.position - transform.position).sqrMagnitude;
            if (distance < closestDistance)
            {
                closestBeacon = beacon;
                closestDistance = distance;
            }
        }

        closestBeacon.SetActive(true);
        foreach (GameObject beacon in beacons)
        {
            if(beacon != closestBeacon)
            {
                beacon.SetActive(false);
            }
        }
    }

    void UpdateCarSpeed(Zone zone)
    {
        if (zone.status == Zone.ZoneStatus.Critical)
            positionPlayback.UpdatePlaybackSpeed(SimulationManager.Speed.Critical);
        else if (zone.status == Zone.ZoneStatus.High)
            positionPlayback.UpdatePlaybackSpeed(SimulationManager.Speed.High);
        else if (zone.status == Zone.ZoneStatus.Medium)
            positionPlayback.UpdatePlaybackSpeed(SimulationManager.Speed.Medium);
        else if (zone.status == Zone.ZoneStatus.Low)
            positionPlayback.UpdatePlaybackSpeed(SimulationManager.Speed.Low);
    }
}
