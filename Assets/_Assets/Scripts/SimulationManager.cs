﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationManager : MonoBehaviour
{
    // Keep track of time of day in a lerp function that continously goes day->day?
    // Keep track of seasons?
    public bool SimulationIsRunning = false;
    public float zoneUpdateInterval = 3.0f; // Should probably be redone to some fuzzy-logic based on time of day etc etc
    [SerializeField] private List<Zone> zones;


    public static SimulationManager instance = null; // Singleton instance

    public static class Speed {
        public static float Critical    = 0.25f;
        public static float High        = 0.50f;
        public static float Medium      = 0.75f;
        public static float Low         = 1.00f;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(this);
        }
        DontDestroyOnLoad(this); // We want the singleton object to persist through scenes
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    [ContextMenu("Gather zones")]
    private void RetrieveAllZones()
    {
        zones = new List<Zone>();
        var zonesInScene = FindObjectsOfType<Zone>();
        foreach(var zone in zonesInScene)
        {
            zones.Add(zone);
        }
    }

    [ContextMenu("Start simulation")]
    private void StartSimulation()
    {
        SimulationIsRunning = true;
        if(zones == null || zones.Count < 1)
        {
            RetrieveAllZones();
        }
        
        foreach (var zone in zones)
        {
            zone.StartZoneSimulation(zoneUpdateInterval);
        }
    }

    [ContextMenu("Stop simulation")]
    private void StopSimulation()
    {
        SimulationIsRunning = false;
    }
}
