﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PositionClip))]
public class PositionPlayback : MonoBehaviour
{
    public float timePlayed;
    public float playbackSpeed = 1.0f;
    public bool loop = false;
    [SerializeField] private bool DEBUG_HACKY_ROTATION;

    private float lastTime;

    [SerializeField] private PositionClip clip;

    bool _playing = false;

    private void Start()
    {
        if (clip == null)
        {
            Debug.LogError("Clip was null");
            clip = GetComponent<PositionClip>();
        }
    }

    IEnumerator Playing()
    {
        do {
            lastTime = Time.timeSinceLevelLoad;
            timePlayed = 0.0f;
            while (timePlayed <= clip.positionNodes[clip.positionNodes.Count - 1].timePoint)
            {
                timePlayed += (Time.timeSinceLevelLoad - lastTime) * playbackSpeed;
                lastTime = Time.timeSinceLevelLoad;

                for (int i = 1; i < clip.positionNodes.Count - 1; i++)
                {
                    if (!_playing) yield break; // Exit playing if done playing

                    if(timePlayed > clip.positionNodes[i].timePoint) // We go further to find the correct lerp spot
                    {
                        continue;
                    }
                    else if (timePlayed <= clip.positionNodes[i].timePoint)
                    {
                        Debug.Log("Came to clip node nr: " + i + " and that nodes position is: " + clip.positionNodes[i].Value);
                        transform.position =    Vector3.Lerp(clip.positionNodes[i - 1].Value, // Previous pos
                                                             clip.positionNodes[i].Value, // Next pos
                                                            (timePlayed - clip.positionNodes[i - 1].timePoint) / (clip.positionNodes[i].timePoint - clip.positionNodes[i - 1].timePoint)); // Current T / position in lerp
                        if (DEBUG_HACKY_ROTATION)
                        {
                            transform.LookAt(clip.positionNodes[i].Value);
                        }
                        break;
                    }
                }
                yield return new WaitForFixedUpdate();
            }

        } while (_playing && loop); // If we are looping we go again
        _playing = false;

    }

    [ContextMenu("Start playback")]
    public void StartPlayback()
    {
        if (clip.positionNodes.Count < 1)
        {
            Debug.LogError("Clip does not contain anything yet!");
            return;
        }
        Debug.Log("Playback started!");

        _playing = true;
        StartCoroutine(Playing());
    }

    [ContextMenu("End playback")]
    public void EndPlayback()
    {
        lastTime = Time.timeSinceLevelLoad;
        timePlayed = 0.0f;

        _playing = false;

        Debug.Log("Playback ended!");
    }

    public void UpdatePlaybackSpeed(float speed)
    {
        playbackSpeed = speed;
    }
}
